package quizapp.wcclg.com.myapplication.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Adapters.AccessControlAdapter;
import quizapp.wcclg.com.myapplication.Adapters.AccessQuestionAdapter;
import quizapp.wcclg.com.myapplication.Adapters.AccessSubCatAdapter;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;
import quizapp.wcclg.com.myapplication.Utils.VolleySingletonQuee;


/**
 * Created by vinod on 9/4/18.
 */

public class Moderator_Control_Questions extends Fragment {

    private RecyclerView recyclerView;
    private AccessQuestionAdapter mAdapter;
    Moderatorbean movie;
    StringReturn stringReturn;
    private List<Moderatorbean> docList = new ArrayList<>();
    Fragment selectedFragment = null;
    Button button,continu;
    public  TextView header;

    public static Moderator_Control_Questions newInstance() {
        Moderator_Control_Questions itemOnFragment = new Moderator_Control_Questions();
        return itemOnFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.access_control_main, container, false);
        getActivity().setTitle("Access Control");
        setHasOptionsMenu(true);
        docList.clear();
      //  session = new SessionManager(getApplicationContext());
        header = view.findViewById(R.id.recycler_view_header);

        header.setText("Choose a Question");


        button=view.findViewById(R.id.restrict_button);
        continu = view.findViewById(R.id.continu);
        continu.setVisibility(View.GONE);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AccessSubCatAdapter.subcatid==null)
                    Toast.makeText(getActivity(),"Please choose Question",Toast.LENGTH_SHORT).show();
                else
                {

                    LayoutInflater li = LayoutInflater.from(getActivity());
                    View promptsView = li.inflate(R.layout.restrict_user_layout, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getActivity());

                    alertDialogBuilder.setView(promptsView);

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    final TextView userInput = promptsView.findViewById(R.id.pop_restrict);

                    final EditText useremail = promptsView.findViewById(R.id.useremail);



                    userInput.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null,
                                    "Please wait...", true);

                            StringRequest stringRequest = new StringRequest(
                                    Request.Method.POST,
                                    URLs.RESTRICT_USERS,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            System.out.println("kkkkkkkk" + response);

                                            try {

                                                JSONObject jsonObject = new JSONObject(response.toString());
                                                String Status = jsonObject.getString("Status");
                                                if (Status.contains("successfully")) {
                                                    progressDialog.cancel();
                                                    Toast.makeText(getActivity(), Status, Toast.LENGTH_SHORT).show();
                                                    alertDialog.dismiss();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<>();

                                    params.put("catid", AccessControlAdapter.catid);
                                    params.put("subcatid",AccessSubCatAdapter.subcatid);
                                    params.put("qnsid",AccessQuestionAdapter.qnsid);
                                    params.put("emailid", useremail.getText().toString());
                                    params.put("type", HomeActivity.type);
                                  //  System.out.println("jgvhgvhj" + params.toString());
                                    return params;
                                }
                            };
                            VolleySingletonQuee.getInstance(getActivity()).addToRequestQueue(stringRequest);

                        }
                    });
                    alertDialog.show();

                }

            }
        });




        recyclerView = view.findViewById(R.id.access_control_recycler);

        mAdapter = new AccessQuestionAdapter(getActivity(),docList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                    getFragmentManager().popBackStack("access_sub_cat", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                    selectedFragment = TakeQUiz.newInstance();
//                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                    transaction.replace(R.id.frame_layout, selectedFragment);
//                    transaction.commit();

                    return true;
                }
                return false;
            }
        });



        stringReturn=new StringReturn();

        System.out.println("hjghjkj"+AccessControlAdapter.catid+" "+AccessSubCatAdapter.subcatid);

        stringReturn.string_return(getActivity(), URLs.URL_GETTING_QNS+"?catid="+AccessControlAdapter.catid+
                "&subcatid="+AccessSubCatAdapter.subcatid+"&quizorsur="+HomeActivity.type, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println("ddddcffvgvg"+result);

                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));
                        movie = new Moderatorbean(jsonObject.getString("quiz_qns"), jsonObject.getString("quiz_id"));
                        docList.add(movie);
                    }


                } catch (JSONException e) {
                    header.setText("Sorry there are no Questions to restrict");
                    button.setEnabled(false);
                    e.printStackTrace();

                }
                mAdapter.notifyDataSetChanged();
            }
        });
        return view;
    }


}

