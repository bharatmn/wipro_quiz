package quizapp.wcclg.com.myapplication.Bean;

/**
 * Created by vinod on 28/3/18.
 */

public class Qnsbean {
    private String qns;
    private String qns_id;
    private String qns_ans;
    private String qns_tooltip;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected = false;



    public Qnsbean(String title, String genre) {
        this.qns = title;
        this.qns_id = genre;
    }

    public String getQns() {
        return qns;
    }

    public void setQns(String qns) {
        this.qns = qns;
    }

    public String getQns_id() {
        return qns_id;
    }

    public void setQns_id(String qns_id) {
        this.qns_id = qns_id;
    }

    public String getQns_ans() {
        return qns_ans;
    }

    public void setQns_ans(String qns_ans) {
        this.qns_ans = qns_ans;
    }

    public String getQns_tooltip() {
        return qns_tooltip;
    }

    public void setQns_tooltip(String qns_tooltip) {
        this.qns_tooltip = qns_tooltip;
    }


}
