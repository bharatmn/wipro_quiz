package quizapp.wcclg.com.myapplication.Fragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Activities.MainActivity;
import quizapp.wcclg.com.myapplication.Activities.NavigatorActivity;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Adapters.TakeQuizAdaapter;
import quizapp.wcclg.com.myapplication.Adapters.TakeSubCatAdapter;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by vinod on 6/4/18.
 */

public class Take_sub_cat extends Fragment {
    private List<Moderatorbean> docList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TakeSubCatAdapter mAdapter;
    StringReturn stringReturn;
    StringRequest stringRequest;
    Moderatorbean movie;
    SessionManager sessionManager;
    static  Context context;
    static File localfile ;

    public static Take_sub_cat newInstance() {
        context=getApplicationContext();
        Take_sub_cat itemOnFragment = new Take_sub_cat();
        return itemOnFragment;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.view_document, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.view_document:

                stringReturn=new StringReturn();

                stringReturn.string_return(getActivity(), URLs.GET_DOCUMENTS+"?catid="+TakeQuizAdaapter.take_quiz_catid+"&quizorsur="+HomeActivity.type, new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {

                        try {

                           JSONArray jsonArray =new JSONArray(result);

                         //   for (int i=0;i<jsonArray.length();i++){

                                JSONObject jsonObject=new JSONObject(jsonArray.getString(0));
String doc_url = jsonObject.getString("doc_url");
                         System.out.println("deeeeee = "+doc_url);
                         //   try {
                   //    URL  url = new URL("ftp://tabuser:Mina4%5t4wXVyouf@103.24.202.5/ModDocuments/"+doc_url);
                            //    System.out.println("deeeeee"+url.getContent().toString());

 //                             FileUtils.copyURLToFile(URL, File)
//                                Uri uri =  Uri.parse( "ftp://tabuser:Mina4%5t4wXVyouf@103.24.202.5/ModDocuments/"+doc_url );
                         //     URLConnection urlc = url.openConnection();
//
                      //        InputStream is = urlc.getInputStream();
//
                          //      URL  url = new URL("http://localhost:8080/GmacWeb/images/dinner-01.jpg");
//FTPClient
                  //   URL url = new URL("ftp://tabuser:Mina4%5t4wXVyouf@103.24.202.5/ModDocuments/05_12_2018__16_38_01_IMG-20180405-WA0003.jpg;type=i");

                       //    Intent browser= new Intent(Intent.ACTION_VIEW, Uri.parse("ftp://tabuser:Mina4%5t4wXVyouf@103.24.202.5/ModDocuments/05_12_2018__16_38_01_IMG-20180405-WA0003.jpg;type=i"));
                          //  Intent browser= new Intent(Intent.ACTION_VIEW, Uri.parse("http://192.168.0.123:8080/GmacWeb/images/dinner-01.jpg"));

                           //  context.startActivity(browser);





//                                String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
//                                String host = "103.24.202.5";
//                                String user = "tabuser";
//                                String pass = "Mina4%5t4wXVyouf";
//                                String filePath = "ModDocuments/05_12_2018__16_38_01_IMG-20180405-WA0003.jpg";
//                                String savePath = "C:\\Users\\user\\Desktop\\testt\\img.jpg";
//
//
//                                ftpUrl = String.format(ftpUrl, user, pass, host, filePath);
//                                System.out.println("URL: " + ftpUrl);
//
//                                try {
//                                    URL url = new URL(ftpUrl);
//                                   URLConnection conn = url.openConnection();
 //                                   InputStream inputStream = conn.getInputStream();

//                                    FileOutputStream outputStream = new FileOutputStream(savePath);
//
//                                    byte[] buffer = new byte[12 * 4096];
//                                    int bytesRead = -1;
//                                    while ((bytesRead = inputStream.read(buffer)) != -1) {
//                                        outputStream.write(buffer, 0, bytesRead);
//                                    }
//
//                                    outputStream.close();
//                                    inputStream.close();
//
//                                    System.out.println("File downloaded");
                              //  }
//                                catch (IOException ex) {
//                                    ex.printStackTrace();
//                                }

















//                                Intent intent = new Intent(Intent.ACTION_VIEW);
//                                intent.setDataAndType(Uri.fromFile(file), "image/jpg");
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                                startActivity(intent);



                           //     http://localhost:8080/GmacWeb/images/dinner-01.jpg
                                //  ftp://103.24.202.5/ModDocuments/05_12_2018__16_38_01_IMG-20180405-WA0003.jpg

//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }


                        } catch (JSONException e) {
                            Toast.makeText(getContext(),"Sorry No Documnet has been Uploaded",Toast.LENGTH_SHORT).show();
                        }
                    }
                });



                break;


        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_documents, container, false);
        getActivity().setTitle("Sub Categories");
        docList.clear();
        recyclerView = view.findViewById(R.id.recycler_view);
        setHasOptionsMenu(true);
        mAdapter = new TakeSubCatAdapter(getActivity(),docList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        movie = new Moderatorbean("Questions under "+TakeQuizAdaapter.take_quiz_name, "cat_id");
        docList.add(movie);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

sessionManager = new SessionManager(getActivity());


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                   getFragmentManager().popBackStack("users", FragmentManager.POP_BACK_STACK_INCLUSIVE);


                    return true;
                }
                return false;
            }
        });



        String emailid = sessionManager.getRegId("email");


        stringReturn=new StringReturn();

        stringReturn.string_return(getActivity(), URLs.URL_GETTING_CAT_USER+"?catid="+TakeQuizAdaapter.take_quiz_catid+"&quizorsur="+HomeActivity.type+
                "&emailid="+emailid, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {

                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){

                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));

                        // System.out.println("lllllllllllllKGHGJGhhh"+jsonObject.getString("doc_type"));

                        movie = new Moderatorbean(jsonObject.getString("sub_cat_name"), jsonObject.getString("sub_cat_id"));
                        docList.add(movie);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });


        return view;
    }
}