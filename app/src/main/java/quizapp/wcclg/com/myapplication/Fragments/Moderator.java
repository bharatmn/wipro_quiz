package quizapp.wcclg.com.myapplication.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Activities.MainActivity;
import quizapp.wcclg.com.myapplication.Activities.NavigatorActivity;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by vinod on 23/1/18.
 */

public class Moderator extends Fragment {
    private List<Moderatorbean> docList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ModeratorAdapter mAdapter;
    StringReturn stringReturn;
    StringRequest stringRequest;
    Moderatorbean movie;
    SessionManager session;
    Fragment selectedFragment = null;


    public static Moderator newInstance() {
        Moderator itemOnFragment = new Moderator();
        return itemOnFragment;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.moderator_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:

                MainActivity.mGoogleSignInClient.signOut()
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                session.logoutUser();
                            }
                        });
                break;

            case R.id.view_user:

                selectedFragment = Moderator_Control.newInstance();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.addToBackStack("moderator");
                transaction.commit();

                break;

            case R.id.user_reappear:

                selectedFragment = User_Requests.newInstance();
                FragmentTransaction user_upload = getActivity().getSupportFragmentManager().beginTransaction();
                user_upload.replace(R.id.frame_layout, selectedFragment);
                user_upload.addToBackStack("moderator");
                user_upload.commit();

                break;

        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_documents, container, false);
        getActivity().setTitle("Categories");
        setHasOptionsMenu(true);
        docList.clear();
        session = new SessionManager(getApplicationContext());

        recyclerView = view.findViewById(R.id.recycler_view);

        mAdapter = new ModeratorAdapter(getActivity(),docList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);

        movie = new Moderatorbean("ADD CAT", "cat_id");
        docList.add(movie);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                  //  getFragmentManager().popBackStack("navigator", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Intent intent = new Intent(getApplicationContext(), NavigatorActivity.class);
                   intent.putExtra("name", "1");
                    startActivity(intent);

                    return true;
                }
                return false;
            }
        });


        stringReturn=new StringReturn();
System.out.println("aaaaaaaaaaa"+HomeActivity.type);
        stringReturn.string_return(getActivity(), URLs.URL_GETTING_CAT_MOD+"?quizorsur="+
                HomeActivity.type+"&mid="+MainActivity.ModeratorId, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println("lllllllllllllKGHGJGhhh"+result);
                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));

                        movie = new Moderatorbean(jsonObject.getString("cat_name"), jsonObject.getString("cat_id"));
                        docList.add(movie);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        return view;
    }

}