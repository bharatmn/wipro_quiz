package quizapp.wcclg.com.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Adapters.AccessControlAdapter;
import quizapp.wcclg.com.myapplication.Adapters.UserRequestAdapter;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.Bean.UserRequestBeanBean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;


/**
 * Created by vinod on 9/4/18.
 */

public class User_Requests extends Fragment {

    private RecyclerView recyclerView;
    private UserRequestAdapter mAdapter;
    UserRequestBeanBean movie;
    StringReturn stringReturn;
    private List<UserRequestBeanBean> docList = new ArrayList<>();
    Fragment selectedFragment = null;
    Button button;
    public static TextView cat,sucat,header;

    public static User_Requests newInstance() {
        User_Requests itemOnFragment = new User_Requests();
        return itemOnFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_requests_main, container, false);
        getActivity().setTitle("User Requests");
        setHasOptionsMenu(true);

      //  session = new SessionManager(getApplicationContext());

        recyclerView = view.findViewById(R.id.user_request_recycler);

        mAdapter = new UserRequestAdapter(getActivity(),docList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                    getFragmentManager().popBackStack("moderator", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                    selectedFragment = TakeQUiz.newInstance();
//                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                    transaction.replace(R.id.frame_layout, selectedFragment);
//                    transaction.commit();

                    return true;
                }
                return false;
            }
        });


        stringReturn=new StringReturn();

        stringReturn.string_return(getActivity(), URLs.ACCESS_CONTROL, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println("lllllllllllllKGHGJGhhh"+result.toString());
                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));
                        System.out.println("lllllllllllllKGHGJGhhh"+jsonObject.toString());

                        movie = new UserRequestBeanBean(jsonObject.getString("name"), jsonObject.getString("email"));
                        docList.add(movie);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        return view;
    }


}

