package quizapp.wcclg.com.myapplication.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Adapters.TakeQuizAdaapter;
import quizapp.wcclg.com.myapplication.Adapters.TakeSubCatAdapter;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;
import quizapp.wcclg.com.myapplication.Utils.VolleySingletonQuee;

/**
 * Created by vinod on 9/4/18.
 */

public class TakingSubCatQns extends Fragment {

    Button imageView,next,pre,review,finish1;
    LinearLayout linearLayout,pre_nex_layout,answer_layout;
    ImageView submit;
    TextView finishQuiz;
    Fragment selectedFragment = null;
    EditText editText;
    ArrayList<String> answers;
    public    boolean doubleBackToExitPressedOnce = false;
    ArrayList<String> questions;
    ArrayList<String> actualanswers;
    ArrayList<String> tool_tip;
    ArrayList<String> Question_Type;
    String cat_iid,subcatid,userpercentage;
    StringReturn stringReturn;
    TextView scoe,time,question;
    Chronometer chronometer;
   	String[] givenanswers;

    public  int currentposition=0;
    LinearLayout dynamic_linearLayout;

    EditText editTextqnsans,datepicker;
    SessionManager sessionManager;

    RadioGroup radioGroup;
    RatingBar ratingBar;
    Spinner spinner;

    RadioButton yes,no;

    public String isclicked ="";

    public static TakingSubCatQns newInstance() {
        TakingSubCatQns itemOnFragment = new TakingSubCatQns();
        return itemOnFragment;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.dashboard, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public static Bitmap takescreenshot(View v) {
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);
        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return b;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dashboard:

                selectedFragment = DashBoard.newInstance();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.addToBackStack("");
                transaction.commit();

                break;

            case R.id.share:

                Bitmap bitmap =   takescreenshot(getView().getRootView());

                if(bitmap!=null)
                    Toast.makeText(getActivity(),"Screen Shot Captured",Toast.LENGTH_SHORT).show();

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), bitmap, "Wipro Quiz App", "created by wiproquiz app");



               // File filePath = getFileStreamPath("shareimage.jpg");  //optional //internal storage
                Intent shareIntent = new Intent();

                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "My Score");
//                shareIntent.putExtra(Intent.EXTRA_TITLE, "Hello 2");
//                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Hello 3");
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));  //optional//use this when you want to send an image
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Post"));


                break;

        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_questios, container, false);
        getActivity().setTitle("Questions");

        imageView =view.findViewById(R.id.image);
        next =view.findViewById(R.id.next);
        pre =view.findViewById(R.id.prev);

        question = view.findViewById(R.id.questions);
        finishQuiz =view.findViewById(R.id.finishtext);
        dynamic_linearLayout = view.findViewById(R.id.view_pager);

      //  review =view.findViewById(R.id.review);
        finish1 =view.findViewById(R.id.finish);
        linearLayout =view.findViewById(R.id.submit_layout);
        pre_nex_layout =view.findViewById(R.id.submit_layout1);
        pre_nex_layout =view.findViewById(R.id.submit_layout1);
        editText =view.findViewById(R.id.answerText);
        submit =view.findViewById(R.id.submit);
        scoe =view.findViewById(R.id.score);
        time =view.findViewById(R.id.time);

        chronometer = (Chronometer)view.findViewById(R.id.chronometer);


        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h   = (int)(time /3600000);
                int m = (int)(time - h*3600000)/60000;
                int s= (int)(time - h*3600000- m*60000)/1000 ;
                String hh = h < 10 ? "0"+h: h+"";
                String mm = m < 10 ? "0"+m: m+"";
                String ss = s < 10 ? "0"+s: s+"";
                cArg.setText(hh+":"+mm+":"+ss);
            }
        });
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        imageView.setVisibility(View.INVISIBLE);
        chronometer.setVisibility(View.INVISIBLE);
        pre.setVisibility(View.INVISIBLE);
        next.setVisibility(View.INVISIBLE);



        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");

                    if(doubleBackToExitPressedOnce)
                    getFragmentManager().popBackStack("takesub", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                  doubleBackToExitPressedOnce = true;
                    Toast.makeText(getActivity(), "Please click BACK again to move to Sub Category", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce=false;
                        }
                    }, 2000);

                    return true;
                }
                return false;
            }
        });


        sessionManager = new SessionManager(getActivity());

       // review.setVisibility(View.INVISIBLE);
        finishQuiz.setVisibility(View.INVISIBLE);

        linearLayout.setVisibility(View.INVISIBLE);
        answers = new ArrayList<>();

        questions = new ArrayList<>();

        actualanswers = new ArrayList<>();
        tool_tip = new ArrayList<>();
        Question_Type = new ArrayList<>();

        cat_iid = TakeQuizAdaapter.take_quiz_catid;

        subcatid = TakeSubCatAdapter.cat_id;


        stringReturn=new StringReturn();

System.out.println("Getting Sub Category vghgvg "+cat_iid+" "+subcatid+" "+HomeActivity.type);



        stringReturn.string_return(getActivity(), URLs.URL_GETTING_QNS+"?catid="+cat_iid+"&subcatid="+subcatid+"&quizorsur="+HomeActivity.type+
                "&mailid="+sessionManager.getRegId("email"), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println("kkkkkkkkkkkkkk"+result);

                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));
                        questions.add(jsonObject.getString("quiz_qns"));
                        actualanswers.add(jsonObject.getString("quiz_ans"));
                        tool_tip.add(jsonObject.getString("quiz_tip_tool"));
                        Question_Type.add(jsonObject.getString("qns_type"));

                    }

                    System.out.println("hjbhjbhb"+actualanswers.toString());

                    System.out.println("hjbhjbhb"+actualanswers.size());

                    System.out.println("sfgghddggddg"+questions+"  "+tool_tip);

                    question.setText((currentposition+1)+". "+questions.get(currentposition));


                    intialize_edittext();
                    intialize_radiogroup();
                    intialize_ratingbar();
                    intialize_datepicker();
                    intialize_spinner();

                    givenanswers = new String[questions.size()];
                    Arrays.fill(givenanswers, "");


                    set_layout(Question_Type.get(currentposition));

                    imageView.setVisibility(View.VISIBLE);
                    chronometer.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);

                    if(Question_Type.size()==1)
                        next.setVisibility(View.INVISIBLE);

                } catch (JSONException e) {

                    finishQuiz.setText("Sorry no question under this category");
                    finishQuiz.setVisibility(View.VISIBLE);

                    e.printStackTrace();

                }




            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                boolean status = fetchfrom_layout(Question_Type.get(currentposition));

                if(status) {

if(!(Arrays.asList(givenanswers).contains("")))
    linearLayout.setVisibility(View.VISIBLE);

                    if (currentposition == questions.size() - 2)
                        next.setVisibility(View.INVISIBLE);


                    currentposition++;

               if(currentposition==questions.size())
               {
                   currentposition--;
                }
                else {

                    pre.setVisibility(View.VISIBLE);

                          }
                    try
                    {
                        question.setText((currentposition + 1) + ". " + questions.get(currentposition));
                        dynamic_linearLayout.removeAllViews();
                        set_layout(Question_Type.get(currentposition));
                    }
                    catch (IndexOutOfBoundsException e)
                    {
                        Toast.makeText(getActivity(),"Sorry there are no more questions",Toast.LENGTH_SHORT).show();
                    }
                }
            }



        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("nnnnn "+currentposition+ " "+questions.size());
                if(currentposition==questions.size()-2)
                    next.setVisibility(View.INVISIBLE);

                pre.setVisibility(View.VISIBLE);

                currentposition++;
                try {
                    question.setText((currentposition + 1) + ". " + questions.get(currentposition));
                    dynamic_linearLayout.removeAllViews();
                    set_layout(Question_Type.get(currentposition));
            }
                catch (IndexOutOfBoundsException e)
            {
                Toast.makeText(getActivity(),"Sorry there are no more questions",Toast.LENGTH_SHORT).show();
            }

            }
        });

        pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(currentposition==1)
                    pre.setVisibility(View.INVISIBLE);

                next.setVisibility(View.VISIBLE);

                currentposition--;

                try
                {
                question.setText((currentposition+1)+". "+questions.get(currentposition));
                dynamic_linearLayout.removeAllViews();
                set_layout(Question_Type.get(currentposition));

            }
                catch (IndexOutOfBoundsException e)
            {
                Toast.makeText(getActivity(),"Sorry there are no more questions",Toast.LENGTH_SHORT).show();
            }

            }
        });

        finish1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Finish Quiz")
                        .setMessage("Are you sure you want to finish the Quiz?")
                        .setPositiveButton("Finish", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

for(int i=0;i<givenanswers.length;i++)
System.out.println("ggg = "+givenanswers[i]);

                                // code which was in submit start
                                chronometer.stop();

                                dynamic_linearLayout.removeAllViews();


                                imageView.setVisibility(View.INVISIBLE);
                                pre.setVisibility(View.INVISIBLE);
                                next.setVisibility(View.INVISIBLE);
                                question.setVisibility(View.INVISIBLE);


                                //         review.setVisibility(View.VISIBLE);
                                finishQuiz.setVisibility(View.VISIBLE);
                                linearLayout.setVisibility(View.VISIBLE);

                                // code which was in submit end

                                userpercentage=getpercentage(givenanswers,actualanswers);
                                finishQuiz.setText("Your Score is "+userpercentage+"%");

                                LinearLayout.LayoutParams finalview = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f);

                                TextView textView = new TextView(getContext());
                                textView.setLayoutParams(finalview);

                                textView.setTextSize(100f);
                                textView.setGravity(Gravity.CENTER);

                                ImageView imageView = new ImageView(getContext());
                                imageView.setLayoutParams(finalview);
                                imageView.setPadding(10,10,10,10);

                                float value = Float.parseFloat(userpercentage);
                                if (value >= 90)
                                {
                                    textView.setText("A+");
                                    imageView.setImageResource(R.drawable.ic_happy);
                                }
                                else if (value>=75)
                                {
                                    textView.setText("A");
                                    imageView.setImageResource(R.drawable.ic_smile);
                                }
                                else if (value>=50)
                                {
                                    textView.setText("B");
                                    imageView.setImageResource(R.drawable.ic_smiling);
                                }
                                else if(value>=31)
                                {
                                    textView.setText("C");
                                    imageView.setImageResource(R.drawable.ic_confused);
                                }
                                else
                                {
                                    textView.setText("D");
                                    imageView.setImageResource(R.drawable.ic_sad);
                                }



                                dynamic_linearLayout.addView(textView);
                                dynamic_linearLayout.addView(imageView);

                                storequiz();



                                setHasOptionsMenu(true);
                                submit.setVisibility(View.INVISIBLE);


                                linearLayout.setVisibility(View.INVISIBLE);


                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setCancelable(false)
                        .show();

            }
        });


        return view;
    }



    private String getpercentage(String[] givenanswers,ArrayList<String> actualanswers)
    {
        int actualans =0;
        int noofvalidqns = 0;
        for(int i=0;i<actualanswers.size();i++)
        {

            if(!(Question_Type.get(i).equals("3")))
            {
                noofvalidqns++;
                if (givenanswers[i].trim().toLowerCase().equals(actualanswers.get(i).trim().toLowerCase())) {
                    actualans++;
                }
            }
        }
        System.out.println("no of valid"+noofvalidqns);
        float number = (float)actualans/(float)noofvalidqns*100;
        String numberAsString = String.format ("%,.2f", number);

        return numberAsString;
    }

    private void intialize_edittext()
    {
        editTextqnsans = new EditText(getContext());
        editTextqnsans.setHint(tool_tip.get(currentposition));
        editTextqnsans.setBackgroundResource(R.drawable.border);
        editTextqnsans.setMinHeight(100);
        editTextqnsans.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        editTextqnsans.setLongClickable(false);
    }

    private void intialize_radiogroup()
    {
        LinearLayout.LayoutParams forvisibletexts = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 2f);
        LinearLayout.LayoutParams forinvisible = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f);


        radioGroup = new RadioGroup(getContext());
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
        radioGroup.setWeightSum(5f);

        RadioButton none = new RadioButton(getContext());
        none.setLayoutParams(forinvisible);
        none.setVisibility(View.INVISIBLE);

         yes = new RadioButton(getContext());
        yes.setLayoutParams(forvisibletexts);
        yes.setText("YES");
        yes.setTag("S");

         no = new RadioButton(getContext());
        no.setText("NO");
        no.setTag("N");
        no.setLayoutParams(forvisibletexts);

        radioGroup.addView(none);
        radioGroup.addView(yes);
        radioGroup.addView(no);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                int selectedid = radioGroup.getCheckedRadioButtonId();
                System.out.println("inside here");
                if(selectedid!=-1)
                {
                    RadioButton radioButton = (RadioButton)radioGroup.findViewById(selectedid);
                    isclicked = radioButton.getTag().toString();
                }
            }
        });

    }

    private void intialize_ratingbar() {

        LinearLayout.LayoutParams ratingbar = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ratingBar = new RatingBar(getContext());
        ratingBar.setLayoutParams(ratingbar);

    }

    private  void intialize_datepicker()
    {
        datepicker= new EditText(getContext());
        datepicker.setHint("Pick your date");
        datepicker.setBackgroundResource(R.drawable.border);
        datepicker.setMinHeight(100);
        datepicker.setClickable(true);
        // datepicker.setEditable(false);
        datepicker.setFocusable(false);

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                datepicker.setText(sdf.format(myCalendar.getTime()));
            }

        };

        datepicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void intialize_spinner()
    {
        spinner = (Spinner) new Spinner(getContext());
        spinner.setBackgroundResource(R.drawable.spinner_border);
        spinner.setMinimumHeight(100);

    }

    private void storequiz()
    {

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                URLs.URL_STOREQUIZE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("kkkkkkkk" + response);

                        Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressBar.setVisibility(View.INVISIBLE);
                        System.out.println("direct qns under cat" + error.toString());

                        //Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();


                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
              //  params.put("quizorsur", HomeActivity.type);
                params.put("email", sessionManager.getRegId("email"));
                params.put("value", userpercentage);
                params.put("time", chronometer.getText().toString());
                params.put("cat_id", TakeQuizAdaapter.take_quiz_catid);
                params.put("sub_cat_id", TakeSubCatAdapter.cat_id);

//System.out.println("hjehdehjd = "+chronometer.getText().toString());

                return params;
            }
        };
        // Add StringRequest to the RequestQueue
        VolleySingletonQuee.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    private void set_layout(String id)
    {
        System.out.println("bbbb = "+givenanswers);
        String storedvalue=givenanswers[currentposition];
        if(!(storedvalue.equals("")))
        {
            imageView.setText("Update");
            imageView.setBackgroundColor(Color.parseColor("#0000ff"));

        }
        else
        {
            imageView.setText("Submit");
            imageView.setBackgroundColor(Color.parseColor("#ff0000"));
        }
        
        switch(id)
        {

            case "1":

                if(!(storedvalue.equals("")))
                    editTextqnsans.setText(storedvalue);
                else editTextqnsans.setText(null);

                editTextqnsans.setHint(tool_tip.get(currentposition));
                dynamic_linearLayout.addView(editTextqnsans);
                break;

            case "2" :

                if(storedvalue.equals("N"))
                  no.setChecked(true);
                else if (storedvalue.equals("S"))
                    yes.setChecked(true);
                else
                    radioGroup.clearCheck();

                dynamic_linearLayout.addView(radioGroup);
                break;

            case "3":

                if(!(storedvalue.equals("")))
                    ratingBar.setRating(Float.parseFloat(storedvalue));
                else
                    ratingBar.setRating(0f);

                dynamic_linearLayout.addView(ratingBar);
                break;

            case "4":

                if(!(storedvalue.equals("")))
                    datepicker.setText(storedvalue);
                else  datepicker.setText(null);

                dynamic_linearLayout.addView(datepicker);
                break;

            case "5":

                List<String> spinnerArray = new ArrayList<String>();

                try {
                    System.out.println("ddffggg"+tool_tip.get(currentposition));
                    JSONObject multiple = new JSONObject(tool_tip.get(currentposition));

            spinnerArray.add("Select");
            spinnerArray.add(multiple.getString("opt1"));
            spinnerArray.add(multiple.getString("opt2"));
            spinnerArray.add(multiple.getString("opt3"));
            spinnerArray.add(multiple.getString("opt4"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        getContext(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);

                spinner.setAdapter(adapter);

                dynamic_linearLayout.addView(spinner);

                   if(!(storedvalue.equals("")))
                spinner.setSelection(Integer.parseInt(storedvalue));

                break;

        }
    }

    private boolean fetchfrom_layout(String id)
    {
        switch(id)
        {
            case "1":

                String ans = editTextqnsans.getText().toString();
                if(ans.equals("")) {
                    Toast.makeText(getActivity(), "Please fill out the field", Toast.LENGTH_LONG).show();
                    return false;
                }
                else {
                    givenanswers[currentposition]= ans;
                    editTextqnsans.setText(null);
                    return true;
                }

            case "2" :

                if(!(isclicked.equals(""))) {
                     givenanswers[currentposition]= isclicked;
                    isclicked="";
                    return true;
                }
                else
                {
                    Toast.makeText(getActivity(), "Please choose yes or no", Toast.LENGTH_LONG).show();
                    return false;
                }

            case "3":
                String rating = String.valueOf(ratingBar.getRating());
            if(rating.equals("0.0"))
            {
                Toast.makeText(getActivity(), "Please rate this question", Toast.LENGTH_LONG).show();
                return false;
            }
            else
            {
                 givenanswers[currentposition]=rating;
                ratingBar.setRating(0f);
                return true;
            }

            case "4":

                String ansd = datepicker.getText().toString();
                if(ansd.equals("")) {
                    Toast.makeText(getActivity(), "Please pick your date", Toast.LENGTH_LONG).show();
                    return false;
                }
                else {
                     givenanswers[currentposition]=ansd ;
                    datepicker.setText(null);
                    return true;
                }
            case "5":

                int selectedpos = spinner.getSelectedItemPosition();

              if(selectedpos!=0)
                   {
                     givenanswers[currentposition]=""+selectedpos;
                       spinner.setSelection(0);
                       return true;
                   }
                   else
              {
                  Toast.makeText(getActivity(), "Please select an option", Toast.LENGTH_LONG).show();
                  return false;
              }


            default: return false;

        }
    }
}

