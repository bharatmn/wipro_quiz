package quizapp.wcclg.com.myapplication.Bean;

/**
 * Created by vinod on 29/1/18.
 */

public class Moderatorbean {
    private String cat_name, cat_id;

    public Moderatorbean() {
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public Moderatorbean(String title, String genre) {
        this.cat_name = title;
        this.cat_id = genre;
    }


}