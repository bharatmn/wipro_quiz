package quizapp.wcclg.com.myapplication.Bean;

/**
 * Created by Admin08 on 6/25/2018.
 */

public class UserRequestBeanBean {
    private String Email,Name;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public UserRequestBeanBean(String Name, String Email) {
        this.Email = Email;
        this.Name = Name;
    }
}
