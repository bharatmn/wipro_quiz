package quizapp.wcclg.com.myapplication.Adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import quizapp.wcclg.com.myapplication.Bean.DashBoardBean;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.Fragments.DashBoard;
import quizapp.wcclg.com.myapplication.R;

/**
 * Created by Admin08 on 6/25/2018.
 */

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.MyViewHolder>{

    Activity activity;
    private List<DashBoardBean> dashList;

    public DashBoardAdapter(Activity activity, List<DashBoardBean> dashList) {
        this.dashList = dashList;
        this.activity=activity;
    }

    @Override
    public DashBoardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboard_contents, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DashBoardAdapter.MyViewHolder holder, int position) {

        final DashBoardBean doc = dashList.get(position);

System.out.println("zzzzzzzzz"+dashList.size());

        holder.sl_no.setText(Integer.toString(position+1));
        holder.email.setText(doc.getEmail());
        holder.score.setText(doc.getScore()+"%");
        holder.time.setText(doc.getTimeTaken());
    }

    @Override
    public int getItemCount() {
        return dashList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView sl_no,email,score,time;


        public MyViewHolder(View view) {
            super(view);
            sl_no = view.findViewById(R.id.sl_no);
            email = view.findViewById(R.id.email);

            score=view.findViewById(R.id.score);
            time=view.findViewById(R.id.timetaken);
        }
    }

}
