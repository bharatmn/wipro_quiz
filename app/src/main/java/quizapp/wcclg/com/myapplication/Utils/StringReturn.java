package quizapp.wcclg.com.myapplication.Utils;

import android.app.Activity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

/**
 * Created by vinod on 14/12/17.
 */

public class StringReturn {
   public static String res;



    public String string_return(final Activity activity,String url,final VolleyCallback callback){
        StringRequest stringRequest;

        // Initialize a new StringRequest
        stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccessResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressBar.setVisibility(View.INVISIBLE);
                        System.out.println("kkkkkkkkkkkkkkkkk"+error.toString());



                    }
                }

        ) {

        };

        // Add StringRequest to the RequestQueue
        VolleySingletonQuee.getInstance(activity).addToRequestQueue(stringRequest);

        return null;
    }
}
