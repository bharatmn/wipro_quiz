package quizapp.wcclg.com.myapplication.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.Bean.UserRequestBeanBean;
import quizapp.wcclg.com.myapplication.Fragments.Moderator_Control;
import quizapp.wcclg.com.myapplication.R;

/**
 * Created by Admin08 on 6/25/2018.
 */

public class UserRequestAdapter extends RecyclerView.Adapter<UserRequestAdapter.MyViewHolder>{

    Activity activity;
    private List<UserRequestBeanBean> dashList;

    public UserRequestAdapter(Activity activity, List<UserRequestBeanBean> dashList) {
        this.dashList = dashList;
        this.activity=activity;
    }

    @Override
    public UserRequestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_request_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserRequestAdapter.MyViewHolder holder, int position) {

        final UserRequestBeanBean doc = dashList.get(position);

        holder.name.setText(doc.getName());
        holder.email.setText(doc.getEmail());

      }

    @Override
    public int getItemCount() {
        return dashList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,email;


        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
email = view.findViewById(R.id.email);
        }
    }

}
