package quizapp.wcclg.com.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Activities.MainActivity;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Adapters.SubCatListAdapter;
import quizapp.wcclg.com.myapplication.Bean.Subcatbean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;

/**
 * Created by vinod on 6/4/18.
 */

public class SubcatlistUnderCat  extends Fragment {
    private List<Subcatbean> docList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SubCatListAdapter mAdapter;
    StringReturn stringReturn;
    StringRequest stringRequest;
    Subcatbean movie;
    Fragment selectedFragment = null;
    SessionManager session;

    public static SubcatlistUnderCat newInstance() {
        SubcatlistUnderCat itemOnFragment = new SubcatlistUnderCat();
        return itemOnFragment;
    }

    @Override
    public void onResume() {
        super.onResume();


    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.sub_cat_moderator, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.upload_document:

                selectedFragment = UploadDocument.newInstance();
                FragmentTransaction transaction_upload = getActivity().getSupportFragmentManager().beginTransaction();
                transaction_upload.replace(R.id.frame_layout, selectedFragment);
                transaction_upload.addToBackStack("subb_cat");
                transaction_upload.commit();

                break;

            case R.id.action_settings:
                session = new SessionManager(getActivity());
                session.logoutUser();
                break;
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_documents, container, false);
        getActivity().setTitle("Sub Categories");
        setHasOptionsMenu(true);
        docList.clear();
        recyclerView = view.findViewById(R.id.recycler_view);

        mAdapter = new SubCatListAdapter(getActivity(),docList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);

        movie = new Subcatbean("Questions Under this CAT", "cat_id");
        docList.add(movie);
        movie = new Subcatbean("ADD SubCategory", "sub_cat_id");
        docList.add(movie);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                    getFragmentManager().popBackStack("category", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    return true;
                }
                return false;
            }
        });





        stringReturn=new StringReturn();

        stringReturn.string_return(getActivity(), URLs.URL_GETTING_CAT_MOD+"?catid="+ModeratorAdapter.selected_cat_id+"&quizorsur="+HomeActivity.type+
                "&mid="+MainActivity.ModeratorId, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {

                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));

                        movie = new Subcatbean(jsonObject.getString("sub_cat_name"), jsonObject.getString("sub_cat_id"));
                        docList.add(movie);

                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        return view;

    }

}