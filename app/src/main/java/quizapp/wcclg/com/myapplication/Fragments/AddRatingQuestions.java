package quizapp.wcclg.com.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import java.util.HashMap;
import java.util.Map;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Adapters.SubCatListAdapter;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleySingletonQuee;


/**
 * Created by vinod on 9/4/18.
 */

public class AddRatingQuestions extends Fragment {

    EditText question,answer,tool_tip;
    Button submit;
    StringRequest stringRequest;
    String q_post,a_post,t_post;
    String s1= ModeratorAdapter.cat_id;
    //String s2= ModeratorAdapter.cat_id;
    Fragment selectedFragment = null;

    public static AddRatingQuestions newInstance() {
        AddRatingQuestions itemOnFragment = new AddRatingQuestions();
        return itemOnFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rating_questions, container, false);


        question = view.findViewById(R.id.qnsText);
//        answer = view.findViewById(R.id.answerText);
//        tool_tip = view.findViewById(R.id.tool_tipText1);
        submit = view.findViewById(R.id.send);
        //This class awesome
        //  awesomeValidation.addValidation(this, R.id.tool_tipText1, RegexTemplate.NOT_EMPTY, R.string.app_name);



        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");

                    Bundle bundle = getArguments();
                    String key = bundle.getString("back");

                    if(key.equals("1"))
                        getFragmentManager().popBackStack("homesurvey", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    else
                        getFragmentManager().popBackStack("subcatsurvey", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    return true;
                }
                return false;
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("direct qns under cat executing");
                // Initialize a new StringRequest
                if (!question.getText().toString().trim().equals("")) {

                    q_post = question.getText().toString().trim();
//                    a_post = answer.getText().toString().trim().toLowerCase();
//                    t_post = tool_tip.getText().toString().trim().toLowerCase();
                    stringRequest = new StringRequest(
                            Request.Method.POST,
                            URLs.URL_POSTING_QUESTION,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    System.out.println("kkkkkkkk" + response);
                                    Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // progressBar.setVisibility(View.INVISIBLE);
                                    System.out.println("direct qns unser cat" + error.toString());

                                    //Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();


                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();

                            params.put("catid", s1);
                            params.put("qns", q_post);
                           // params.put("ans", a_post);
                            // params.put("tip", null);

                            if( SubCatListAdapter.selected_sub_catid == null) {
                                params.put("subcatid", "0");
                            }
                            else {
                                params.put("subcatid", SubCatListAdapter.selected_sub_catid);
                            }

                            params.put("qnsid","3");
                            params.put("quizorsur", HomeActivity.type);


                            return params;
                        }
                    };
                    // Add StringRequest to the RequestQueue
                    VolleySingletonQuee.getInstance(getActivity()).addToRequestQueue(stringRequest);

                }

                else
                {
                    Toast.makeText(getActivity(),"Please type the question",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
}

