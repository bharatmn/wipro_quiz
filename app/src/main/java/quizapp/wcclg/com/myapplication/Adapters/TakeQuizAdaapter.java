package quizapp.wcclg.com.myapplication.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;

import java.util.List;
import java.util.Random;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.Fragments.Take_sub_cat;
import quizapp.wcclg.com.myapplication.R;

/**
 * Created by vinod on 28/3/18.
 */

public class TakeQuizAdaapter extends RecyclerView.Adapter<TakeQuizAdaapter.MyViewHolder> {

    private List<Moderatorbean> docList;
    Activity activity;
    AlertDialog alertDialog;
    StringRequest stringRequest;
    String cat_name;
    public  static  String cat_id;
    Fragment selectedFragment = null;
    String[] randomcolor={"#00a1f1","#f65314","#ffbb00","#EC274E","#A4C639"};
    int [] randomicons={R.drawable.ic_question,R.drawable.ic_logic_board_games,R.drawable.ic_sun,R.drawable.ic_customer_service};

    int count;
    public static String take_quiz_catid;
    public static String take_quiz_name;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, url;
        ImageView imageView;
        public CardView cardView;
        LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.title);
            imageView = view.findViewById(R.id.icon);
            cardView=view.findViewById(R.id.card_view);
            linearLayout=view.findViewById(R.id.line1);
        }
    }


    public TakeQuizAdaapter(Activity activity, List<Moderatorbean> moviesList) {
        this.docList = moviesList;
        this.activity=activity;
    }
    public int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }
    @Override
    public TakeQuizAdaapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doc_list_row, parent, false);

        int height = parent.getHeight() / 4;
        itemView.setMinimumHeight(height);




        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TakeQuizAdaapter.MyViewHolder holder, int position) {
        final Moderatorbean doc = docList.get(position);
        holder.name.setText(doc.getCat_name());
        count++;


        if (count >= randomicons.length) {
            count = 0;


        }



        holder.imageView.setImageResource(randomicons[count]);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                take_quiz_catid=doc.getCat_id();
                take_quiz_name=doc.getCat_name();

                /*    Intent intent=new Intent(activity.getApplicationContext(),TakeSubCatActivity.class);
                    intent.putExtra("cat_id", doc.getCat_id());
                    activity.startActivity(intent);*/
                selectedFragment = Take_sub_cat.newInstance();
                FragmentTransaction transaction = ((HomeActivity) activity).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.addToBackStack("users");
                transaction.commit();


            }
        });







    }

    @Override
    public int getItemCount() {
        return docList.size();
    }
}