package quizapp.wcclg.com.myapplication.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Activities.MainActivity;
import quizapp.wcclg.com.myapplication.Activities.NavigatorActivity;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Adapters.TakeQuizAdaapter;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by vinod on 28/3/18.
 */

public class TakeQUiz extends Fragment {
    private List<Moderatorbean> docList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TakeQuizAdaapter mAdapter;
    StringReturn stringReturn;
    StringRequest stringRequest;
    Moderatorbean movie;
    SessionManager session;


    public static TakeQUiz newInstance() {
        TakeQUiz itemOnFragment = new TakeQUiz();
        return itemOnFragment;
    }

    @Override
    public void onResume() {
        super.onResume();


    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.naviagtion_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
              // session.logoutUser();
                System.out.println("google sign in pppppppppppppppp");

if( MainActivity.mGoogleSignInClient!=null)
                MainActivity.mGoogleSignInClient.signOut()
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                session.logoutUser();
System.out.println("google sign in signout");
                                // ...
                            }
                        });
else
    session.logoutUser();

break;
//                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//
//                            }
//                        });                break;


        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_documents, container, false);
        getActivity().setTitle("Categories");
        setHasOptionsMenu(true);
        session = new SessionManager(getApplicationContext());
        docList.clear();

        // for take quiz HomeActivity.type  = 1

        System.out.println("sdsdsdsdsds"+HomeActivity.type);

        recyclerView = view.findViewById(R.id.recycler_view);

        mAdapter = new TakeQuizAdaapter(getActivity(),docList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 2);

        recyclerView.setLayoutManager(mLayoutManager);

       // movie = new Moderatorbean("ADD CAT", "cat_id");
       // docList.add(movie);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);





        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                    //  getFragmentManager().popBackStack("navigator", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Intent intent = new Intent(getApplicationContext(), NavigatorActivity.class);

                    intent.putExtra("name", "2");
                    // intent.putExtra("type","1");

                    startActivity(intent);

                    return true;
                }
                return false;
            }
        });





        stringReturn=new StringReturn();

        String emailid = session.getRegId("email");

        System.out.println("hjjhjh"+HomeActivity.type+" "+emailid);
      //  System.out.println("hfdhdhdhhdh"+);

// need to check with data transfer
        stringReturn.string_return(getActivity(), URLs.URL_GETTING_CAT_USER+"?quizorsur="+HomeActivity.type+"&emailid="+emailid, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {

                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));
                        // System.out.println("lllllllllllllKGHGJGhhh"+jsonObject.getString("doc_type"));

                        movie = new Moderatorbean(jsonObject.getString("cat_name"), jsonObject.getString("cat_id"));
                        docList.add(movie);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });
        return view;
    }
}
