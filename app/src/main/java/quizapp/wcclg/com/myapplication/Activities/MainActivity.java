package quizapp.wcclg.com.myapplication.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
//import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import quizapp.wcclg.com.myapplication.Fragments.AddingQuestions;
import quizapp.wcclg.com.myapplication.Fragments.TakeQUiz;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleySingletonQuee;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 007;
    SignInButton signInButton;
    public static  String ModeratorId;
   public static GoogleSignInClient mGoogleSignInClient;

    CallbackManager callbackManager;
    LoginButton loginButton;
    private static final String EMAIL = "email";
    StringRequest stringRequest;
    SessionManager session;

    @Override
    protected void onStart() {
        super.onStart();
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
    }

    // related to GOOGLE SIGNIN --fb
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    // related to GOOGLE SIGNIN
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            login_post(account.getEmail(),account.getDisplayName());

        } catch (ApiException e) {
        }
    }

// related to GOOGLE SIGNIN
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    // related to GOOGLE SIGNIN
    //code to signout has integrated in logout navigatoractivity class
//    public void signOut() {
//        mGoogleSignInClient.signOut()
//                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        System.out.println("U have been Signed Out");
//                    }
//                });
//    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        callbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(MainActivity.this, gso);




        // related to FB SIGNIN
        loginButton = (LoginButton) findViewById(R.id.facebook_login_button);
        try {
            loginButton.setReadPermissions(Arrays.asList(EMAIL));
        } catch (Exception e) {
            e.printStackTrace();
        }


        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        // related to FB SIGNIN
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                // facebook result

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject jsonObject,
                                                    GraphResponse response) {

                                try {
                                 login_post(jsonObject.getString("email"),jsonObject.getString("first_name"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


        // related to GOOGLE SIGNIN

        // Set the dimensions of the sign-in button.
        signInButton = findViewById(R.id.google_sign_in_button);
       // s_out = findViewById(R.id.sign_out_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        findViewById(R.id.google_sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = view.getId();

                switch (id) {
                    case R.id.google_sign_in_button:
                        System.out.println("ggggggggggggggggggggggg");
                        // from google sign in execution starts
                   signIn();
                        break;
                }
            }
        });



        findViewById(R.id.mod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(MainActivity.this);

                View sheetView = getLayoutInflater().inflate(R.layout.fragment_history_bottom_sheet, null);

                mBottomSheetDialog.setContentView(sheetView);
                mBottomSheetDialog.show();

                final EditText userInput = sheetView
                        .findViewById(R.id.email);
                final EditText userInput1 = sheetView
                        .findViewById(R.id.pass);

                userInput.setText("test1@test.com");
                userInput1.setText("test1");

                final Button userInputbutton = sheetView
                        .findViewById(R.id.submit);
                userInputbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //session.createLoginSession("123", "123");
                        // session.saveRegId("1");
                        String semail=userInput.getText().toString().toLowerCase();
                        String spass=userInput1.getText().toString().toLowerCase();

                        if(semail.equals(""))
                            Toast.makeText(MainActivity.this, "Please enter your EmailId", Toast.LENGTH_SHORT).show();
                        else if(!(Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE).matcher(semail).find()))
                            Toast.makeText(MainActivity.this, "Please enter valid EmailId", Toast.LENGTH_SHORT).show();
                        else if(spass.equals(""))
                            Toast.makeText(MainActivity.this, "Please enter your Password", Toast.LENGTH_SHORT).show();
                        else
                            check_moderator_login(semail,spass);



                    }
                });

            }
        });


    }


    public void login_post(final String email, final String name){

        stringRequest = new StringRequest(
                Request.Method.POST,
                URLs.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                         String status =   jsonObject.getString("Status");

                         if(status.contains("elcome")) {
                             Toast.makeText(MainActivity.this, "Welcome, " + name, Toast.LENGTH_LONG).show();
                             session.createLoginSession(email, name);
                             Intent intent = new Intent(getApplicationContext(), NavigatorActivity.class);
                             intent.putExtra("name", "2");
                             startActivity(intent);
                         }
                         else
                         {
                             Toast.makeText(MainActivity.this, status, Toast.LENGTH_LONG).show();
                         }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email.replace("@googlemail.com","@gmail.com"));
                params.put("name", name);

                return params;
            }
        };
        // Add StringRequest to the RequestQueue
        VolleySingletonQuee.getInstance(MainActivity.this).addToRequestQueue(stringRequest);
    }

    public void check_moderator_login(final String email, final String name){

        stringRequest = new StringRequest(
                Request.Method.POST,
                URLs.MODERATOR_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("hfghfhf"+response);
                            JSONObject jsonObject = new JSONObject(response);
                         String status = jsonObject.getString("Status");

                            if (status.contains("successfully")) {

                                ModeratorId = jsonObject.getString("Id");

                                Toast.makeText(MainActivity.this, status, Toast.LENGTH_LONG).show();
                                Intent intent=new Intent(getApplicationContext(),NavigatorActivity.class);
                                intent.putExtra("name", "1");
                                startActivity(intent);

                              //  System.out.println("hjgghjghjghj = "+MainActivity.ModeratorId);

                            }else {
                                Toast.makeText(MainActivity.this, status, Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

System.out.println("bnbnbnnb"+error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("pass", name);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(60),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add StringRequest to the RequestQueue
        VolleySingletonQuee.getInstance(MainActivity.this).addToRequestQueue(stringRequest);

    }



}
