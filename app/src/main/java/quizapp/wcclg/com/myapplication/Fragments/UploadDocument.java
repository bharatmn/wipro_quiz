package quizapp.wcclg.com.myapplication.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.transition.Visibility;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Activities.MainActivity;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Adapters.SubCatListAdapter;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyMultipartRequest;
import quizapp.wcclg.com.myapplication.Utils.VolleySingletonQuee;

/**
 * Created by vinod on 9/4/18.
 */

public class UploadDocument extends Fragment {

    StringRequest stringRequest;
    TextView path,upload;
    ImageView imageView;

    String s1= ModeratorAdapter.cat_id;
    Fragment selectedFragment = null;
    int PICK_IMAGE_REQUEST=111;
    Button as;
    String displayName = "";
    Uri filePath;
    long filelength;


    public static UploadDocument newInstance() {
        UploadDocument itemOnFragment = new UploadDocument();
        return itemOnFragment;
    }

    public static boolean checkPermission(String strPermission, Context _c, Activity _a) {
        int result = ContextCompat.checkSelfPermission(_c, strPermission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
//





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
     //   super.onActivityResult(requestCode, resultCode, data);
path.setText(null);
        imageView.setImageDrawable(null);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            String uriString = filePath.toString();

            // String fileextension =  filename.substring(filename.lastIndexOf(".") + 1, filename.length());

          File  myFile = new File(uriString);

            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = getActivity().getContentResolver().query(filePath, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));



                        int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                       // cursor.moveToFirst();
                        filelength = (int) cursor.getLong(sizeIndex);


                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
            }

            if(!displayName.toLowerCase().contains(".pdf"))
            {
                imageView.setImageResource(R.drawable.ic_doc);
            }
            else
            {
                imageView.setImageResource(R.drawable.ic_pdf);
            }

            System.out.println("filePathfilePath = " + displayName );

            path.setText(displayName);
            upload.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            // String fileName = Uri.parse(url).getLastPathSegment();
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_pick, container, false);
        getActivity().setTitle("Upload Document");

        as = view.findViewById(R.id.fetch);
        upload=view.findViewById(R.id.upload);
        path=view.findViewById(R.id.path);

        imageView = view.findViewById(R.id.doc_image);

        upload.setVisibility(view.INVISIBLE);

        as.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getApplicationContext(), getActivity())) {


                    String[] mimeTypes =
                            {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
//                                    "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
//                                    "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                    // "text/plain",
                                    "application/pdf"
                                    // ,"application/zip"
                            };

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
                        if (mimeTypes.length > 0) {
                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        }
                    } else {
                        String mimeTypesStr = "";
                        for (String mimeType : mimeTypes) {
                            mimeTypesStr += mimeType + "|";
                        }
                        intent.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
                    }


                    startActivityForResult(Intent.createChooser(intent, "Select Document"), PICK_IMAGE_REQUEST);

                    Log.v("permission", "Permission is granted");
                } else {
                    Log.v("permission", "Permission is revoked");
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }

            }
        });



        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             System.out.println("file size = "+filelength);

                if((float)filelength/1024 <= 1024 )
                //    Toast.makeText(getActivity(),"File is below 1 MB",Toast.LENGTH_SHORT).show();
             uploadDocument(filePath,MainActivity.ModeratorId);
                else
                 Toast.makeText(getActivity(),"Please upload file below 1 MB",Toast.LENGTH_SHORT).show();
            }
        });


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                    getFragmentManager().popBackStack("subb_cat", FragmentManager.POP_BACK_STACK_INCLUSIVE);


                    return true;
                }
                return false;
            }
        });

        return view;
    }


    private void uploadDocument(final Uri filecontent, final String moderatorid) {
        final ProgressDialog  progressDialog = ProgressDialog.show(getActivity(), "",
                "Uploading. Please wait...");
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, URLs.UPLOAD_DOCUMENT,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                              Toast.makeText(getActivity(), obj.getString("Status"), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("modid", moderatorid);
                params.put("catid", MainActivity.ModeratorId);
                params.put("quizorsur", HomeActivity.type);

                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData() throws IOException {
                Map<String, DataPart> params = new HashMap<>();

                params.put("doc", new DataPart(displayName,getBytes(getActivity().getContentResolver().openInputStream(filecontent))));
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingletonQuee.getInstance(getActivity()).addToRequestQueue(volleyMultipartRequest);
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        while ( inputStream.read(buffer) != -1) {
            byteBuffer.write(buffer);
        }
        return byteBuffer.toByteArray();
    }
}

