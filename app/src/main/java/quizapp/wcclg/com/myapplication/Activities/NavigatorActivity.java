package quizapp.wcclg.com.myapplication.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import quizapp.wcclg.com.myapplication.Fragments.Moderator_Control;
import quizapp.wcclg.com.myapplication.Fragments.UploadDocument;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Admin08 on 6/15/2018.
 */

public class NavigatorActivity extends AppCompatActivity {
    ImageButton quiz,survey;
TextView quiztype,surveytype;
    SessionManager session;

    String role;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_page);

//        getActivity().setTitle("Categories");
//        setHasOptionsMenu(true);

        quiz = (ImageButton) findViewById(R.id.quiz);
        survey = (ImageButton) findViewById(R.id.survey);

        quiztype = (TextView) findViewById(R.id.quiztext);
        surveytype = (TextView) findViewById(R.id.surveytext);



        session = new SessionManager(getApplicationContext());

        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            role =(String) b.get("name");
        }

        if(role.equals("1"))
        {
            quiztype.setText("Manage Quiz");
            surveytype.setText("Manage Survey");

            quiz.setImageResource(R.drawable.quiz_qns);
            survey.setImageResource(R.drawable.survey_qns);
        }
        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);

                            intent.putExtra("name", role);
                            intent.putExtra("type","1");

                            startActivity(intent);
            }
        });

        survey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.putExtra("name", role);
                intent.putExtra("type","2");
                startActivity(intent);

            }
        });
    }

    boolean doubleBackToExitPressedOnce = false;


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                this.finishAffinity();
            }
            // super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.naviagtion_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:

                // -----> IMP : - was used to sign out from gamil if user logs in through facebook

                MainActivity.mGoogleSignInClient.signOut()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                session.logoutUser();
                            }
                        });
                break;

        }
        return true;

    }
}
