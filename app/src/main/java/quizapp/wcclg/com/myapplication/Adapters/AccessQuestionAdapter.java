package quizapp.wcclg.com.myapplication.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.R;

/**
 * Created by Admin08 on 6/25/2018.
 */

public class AccessQuestionAdapter extends RecyclerView.Adapter<AccessQuestionAdapter.MyViewHolder>{

    Activity activity;
    private List<Moderatorbean> dashList;
    public int selected=-1;
    public static String qnsid;

    public AccessQuestionAdapter(Activity activity, List<Moderatorbean> dashList) {
        this.dashList = dashList;
        this.activity=activity;
    }

    @Override
    public AccessQuestionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.access_control_contents, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AccessQuestionAdapter.MyViewHolder holder, final int position) {

        final Moderatorbean doc = dashList.get(position);

        holder.category_name.setText(Integer.toString(position+1)+". "+doc.getCat_name());
        holder.category_name.setBackgroundResource(selected==position?R.drawable.selected_border:R.drawable.border);

        holder.category_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qnsid=doc.getCat_id();
                selected=position;
                notifyDataSetChanged();
            }
        });

      }

    @Override
    public int getItemCount() {
        return dashList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView category_name;


        public MyViewHolder(View view) {
            super(view);
            category_name = view.findViewById(R.id.category_name);
            category_name.setGravity(Gravity.LEFT);
        }
    }

}
