package quizapp.wcclg.com.myapplication.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;

import java.util.List;
import java.util.Random;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Bean.Qnsbean;
import quizapp.wcclg.com.myapplication.Fragments.AddingQuestionToSubCat;
import quizapp.wcclg.com.myapplication.Fragments.AddingQuestions;
import quizapp.wcclg.com.myapplication.Fragments.SubQnsAddlist;
import quizapp.wcclg.com.myapplication.R;

/**
 * Created by vinod on 28/3/18.
 */

public class QnsAdapter extends RecyclerView.Adapter<QnsAdapter.MyViewHolder> {

    private List<Qnsbean> docList;
    Activity activity;
    public  static String Selectedqnsid="-1";
    private int lastCheckedPosition = -1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, url;
        TextView textView;

        RadioButton radioButton;


        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.title);
            textView = view.findViewById(R.id.no);
            radioButton = view.findViewById(R.id.radio);
        }
    }


    public QnsAdapter(Activity activity, List<Qnsbean> moviesList) {
        this.docList = moviesList;
        this.activity=activity;
    }

    @Override
    public QnsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.qns_list_row, parent, false);
     //   int height = parent.getHeight() / 4;
        //itemView.setMinimumHeight(height);
        itemView.setMinimumWidth(parent.getWidth());
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final QnsAdapter.MyViewHolder holder, final int position) {
        final Qnsbean doc = docList.get(position);


        holder.name.setText("Q."+(position+1)+" "+doc.getQns());

        holder.radioButton.setChecked(position == lastCheckedPosition);
//
        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("nnn = " + (position == lastCheckedPosition));

                if (position == lastCheckedPosition) {
                    lastCheckedPosition=-1;
                    Selectedqnsid="-1";
                }
                else
                {
                    lastCheckedPosition = position;
                Selectedqnsid = doc.getQns_id();
                }

                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        System.out.println("docList.size() = "+docList.size());
        return docList.size();
    }
}