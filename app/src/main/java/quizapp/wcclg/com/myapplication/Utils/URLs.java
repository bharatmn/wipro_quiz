package quizapp.wcclg.com.myapplication.Utils;

/**
 * Created by vinod on 28/11/17.
 */

public class URLs {

// 103.24.202.5 is wipro server ip
    // my local ip  192.168.0.38 for lanshree
    // my ip 192.168.43.81
    // 192.168.0.94 omgana

    // wipro IP 192.168.1.100

    // xohri 192.168.0.123


    private static final String ROOT_URL1 = "http://103.24.202.5:8080/Wipro/";

    private static final String ROOT_URL14 = "http://192.168.0.123:8080/Wipro/";
    private static final String ROOT_URL13 = "http://192.168.43.243:8080/Wipro/";
    private static final String ROOT_URL12 = "http://192.168.0.94:8080/Wipro/";
    private static final String ROOT_URL11 = "http://192.168.43.81:8080/Wipro/";

    public static final String URL_GETTING_CAT_USER= ROOT_URL1 + "quizlistuser";
    public static final String URL_GETTING_CAT_MOD= ROOT_URL1 + "quizlistmoderator";
    public static final String URL_GETTING_QNS= ROOT_URL1 + "qnslist";
    public static final String URL_GETTING_NEWCAT= ROOT_URL1 + "quizcatup";
    public static final String URL_POSTING_QUESTION= ROOT_URL1 + "quizup";
    public static final String URL_LOGIN= ROOT_URL1 + "savequizuser";

    public static final String URL_DELEATE_ITEM = ROOT_URL1 + "delquiz";
    public static final String URL_DELETE_CAT = ROOT_URL1 + "deletecat";
    public static final String URL_STOREQUIZE = ROOT_URL1 + "storequiz";
    public static final String DASH_BOARD= ROOT_URL1 + "dashboard";
    public static final String ACCESS_CONTROL= ROOT_URL1 + "getuser";
    public static final String RESTRICT_USERS= ROOT_URL1 + "restrictusers";
    public static final String MODERATOR_LOGIN= ROOT_URL1 + "checkmoderatorlogin";
    public static final String UPLOAD_DOCUMENT= ROOT_URL1 + "docup";
    public static final String GET_DOCUMENTS= ROOT_URL1 + "mydocs";

}

