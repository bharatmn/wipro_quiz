package quizapp.wcclg.com.myapplication.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;

import java.util.List;
import java.util.Random;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.Fragments.TakingSubCatQns;
import quizapp.wcclg.com.myapplication.R;

/**
 * Created by vinod on 6/4/18.
 */

public class TakeSubCatAdapter extends RecyclerView.Adapter<TakeSubCatAdapter.MyViewHolder> {

    private List<Moderatorbean> docList;
    Activity activity;
    public  static  String cat_id;
    Fragment selectedFragment = null;
    String[] randomcolor={"#00a1f1","#f65314","#ffbb00","#EC274E","#A4C639"};
    int [] randomicons={R.drawable.ic_question,R.drawable.ic_logic_board_games,R.drawable.ic_sun,R.drawable.ic_customer_service};
    int count;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, url;
        ImageView imageView;
        public CardView cardView;
        LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.title);
            imageView = view.findViewById(R.id.icon);
            // url = (TextView) view.findViewById(R.id.genre);
            cardView=view.findViewById(R.id.card_view);
            linearLayout=view.findViewById(R.id.line1);
        }
    }


    public TakeSubCatAdapter(Activity activity, List<Moderatorbean> moviesList) {
        this.docList = moviesList;
        this.activity=activity;
    }



    @Override
    public TakeSubCatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doc_list_row, parent, false);

        int height = parent.getHeight() / 4;
        itemView.setMinimumHeight(height);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TakeSubCatAdapter.MyViewHolder holder, int position) {
        final Moderatorbean doc = docList.get(position);
        holder.name.setText(doc.getCat_name());

        count++;
        System.out.println("Sequentialrepeating"+count);


        if (count >= randomicons.length) {
            count = 0;
            System.out.println("Sequentialrepeatingcountinside if" + count);


        }


        System.out.println("Sequentialrepeatingcountinside ifrandomcolor[count]" + randomcolor[count]);

        holder.imageView.setImageResource(randomicons[count]);

        if (doc.getCat_id().equals("cat_id")){
            //holder.imageView.setImageResource(R.drawable.ic_icons8_plus);
            holder.linearLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
            holder.name.setText(doc.getCat_name());
            holder.name.setTextColor(Color.parseColor("#000000"));
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cat_id="0";

                if (doc.getCat_id().equals("cat_id")){
                    selectedFragment = TakingSubCatQns.newInstance();

                }else {
                    cat_id=doc.getCat_id();
                    selectedFragment = TakingSubCatQns.newInstance();
                }

                FragmentTransaction transaction = ((HomeActivity) activity).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.addToBackStack("takesub");
                transaction.commit();
            }
        });







    }

    @Override
    public int getItemCount() {
        return docList.size();
    }
}