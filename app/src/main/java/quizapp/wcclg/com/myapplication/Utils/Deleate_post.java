package quizapp.wcclg.com.myapplication.Utils;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Activities.MainActivity;

/**
 * Created by vinod on 9/4/18.
 */

public class Deleate_post {
  public static StringRequest stringRequest;

    public static void deleate_item_post(Activity activity, final String type,  final String catid, final String subcatid,final String qnsid,final VolleyCallback callback){

        stringRequest = new StringRequest(
                Request.Method.POST,
                URLs.URL_DELEATE_ITEM,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccessResponse(response);
                        System.out.println("llllllllldelete"+response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("llllllllldelete"+error);


                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("type", type);
                params.put("catid", catid);
                params.put("subcatid", subcatid);
                params.put("qnsid", qnsid);
                params.put("quizorsur",HomeActivity.type);

System.out.println("ddddddddd = "+params.toString());
                return params;
            }
        };
        // Add StringRequest to the RequestQueue
        VolleySingletonQuee.getInstance(activity).addToRequestQueue(stringRequest);
    }
}

