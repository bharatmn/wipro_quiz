package quizapp.wcclg.com.myapplication.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.IOException;

import quizapp.wcclg.com.myapplication.Fragments.Moderator;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Fragments.TakeQUiz;


public class HomeActivity extends AppCompatActivity  {
    Fragment selectedFragment = null;
    String role;
    public static String type;
    SessionManager session;
    boolean isconnectedactive;


    public boolean isConnected()  {
        final String command = "ping -c 1 google.com";
        try {
            return Runtime.getRuntime().exec(command).waitFor() == 0;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        isconnectedactive=isConnected();

        registerInternetCheckReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }
    /**
     *  Method to register runtime broadcast receiver to show snackbar alert for internet connection..
     */
    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
       // internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        session = new SessionManager(getApplicationContext());
        // AccessToken accessToken = AccessToken.getCurrentAccessToken();

        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        type = (String) b.get("type");
        if (b != null) {
            role = (String) b.get("name");
        }

        if (role.equals("1")) {
            selectedFragment = Moderator.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack("navigator");
            transaction.replace(R.id.frame_layout, selectedFragment);
            transaction.commit();
        } else if (role.equals("2")) {
            selectedFragment = TakeQUiz.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack("navigator");
            transaction.replace(R.id.frame_layout, selectedFragment);
            transaction.commit();

        }

    }

      public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
       @Override
       public void onReceive(Context context, Intent intent) {

//           int conn = getConnectivityStatus(context);
//           String status = null;
//           if (conn == TYPE_WIFI) {
//               status = "Wifi enabled";
//           } else if (conn == TYPE_MOBILE) {
//               status = "Mobile data enabled";
//           } else if (conn == TYPE_NOT_CONNECTED) {
//               status = "Not connected to Internet";
//           }
//           return status;

           if(isConnected())
           {
               if(!isconnectedactive) {
                   Toast.makeText(getBaseContext(), "Connection received", Toast.LENGTH_SHORT).show();
                   isconnectedactive=true;
               }

           }
           else
           {
               Toast.makeText(getBaseContext(),"Connection lost",Toast.LENGTH_SHORT).show();
               isconnectedactive=false;
           }

       }
       };

         }
