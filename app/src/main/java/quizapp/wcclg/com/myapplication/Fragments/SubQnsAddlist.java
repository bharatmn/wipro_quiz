package quizapp.wcclg.com.myapplication.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Adapters.QnsAdapter;
import quizapp.wcclg.com.myapplication.Adapters.SubCatListAdapter;
import quizapp.wcclg.com.myapplication.Bean.Qnsbean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.Deleate_post;
import quizapp.wcclg.com.myapplication.Utils.SessionManager;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;

/**
 * Created by vinod on 6/4/18.
 */

public class SubQnsAddlist extends Fragment {

    static String a;
    StringReturn stringReturn;
    private QnsAdapter mAdapter;
    StringRequest stringRequest;
    Qnsbean movie;
    private List<Qnsbean> docList = new ArrayList<>();
    private RecyclerView recyclerView;
    FragmentTransaction transaction;

    String s1= ModeratorAdapter.selected_cat_id;
    String s2= SubCatListAdapter.selected_sub_catid;
    Fragment selectedFragment = null;
    SessionManager sessionManager;
    Button addqns;

    public static SubQnsAddlist newInstance() {
        SubQnsAddlist itemOnFragment = new SubQnsAddlist();
        QnsAdapter.Selectedqnsid="-1";
        return itemOnFragment;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.deleating, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.play:

                if(  QnsAdapter.Selectedqnsid.equals("-1"))
                    Toast.makeText(getContext(),"Please select a question",Toast.LENGTH_SHORT).show();
                else
                Deleate_post.deleate_item_post(getActivity(), "3", ModeratorAdapter.selected_cat_id, SubCatListAdapter.selected_sub_catid, QnsAdapter.Selectedqnsid, new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {

                        selectedFragment = SubQnsAddlist.newInstance();
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.addToBackStack("subcat");
                        transaction.commit();

                    }
                });


                break;


        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_quiz_list, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        addqns = view.findViewById(R.id.btnqns);
        docList.clear();
        getActivity().setTitle("QUESTIONS");

        setHasOptionsMenu(true);
        mAdapter = new QnsAdapter(getActivity(),docList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
recyclerView.setHasFixedSize(true);

        sessionManager = new SessionManager(getActivity());

       // movie = new Qnsbean("ADD QUESTIONS", "subcat_id");
       /// docList.add(movie);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
       recyclerView.setAdapter(mAdapter);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                    getFragmentManager().popBackStack("subcat", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    return true;
                }
                return false;
            }
        });

        final Bundle bundle = new Bundle();
        bundle.putString("back","2");

        stringReturn=new StringReturn();
        post();

        addqns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(HomeActivity.type.equals("2"))
                {
                    LayoutInflater inflater =getActivity().getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.question_list_out, null);

                    final RadioGroup radioGroup=(RadioGroup)alertLayout.findViewById(R.id.select_question_type);

                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                    {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch(checkedId){
                                case R.id.question_answer:
                                    a="1";
                                    break;
                                case R.id.yes_no:
                                    a="2";
                                    break;
                                case R.id.rating:
                                    a="3";
                                    break;
                                case R.id.date:
                                    a="4";
                                    break;
                                case R.id.dropdown:
                                    a="5";
                                    break;
                            }
                        }
                    });


                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    //    alert.setTitle("Info");
                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(false);


                    alert.setNegativeButton("Cancel",null);

                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            if(a.equals("1"))
                            {
                                selectedFragment = AddingQuestionToSubCat.newInstance();
                            }
                            else if(a.equals("2"))
                            {
                                selectedFragment = YesNoTypeQuestions.newInstance();
                            }
                            else if(a.equals("3"))
                            {
                                selectedFragment = AddRatingQuestions.newInstance();
                            }
                            else if(a.equals("4"))
                            {
                                selectedFragment = DateTypeQuestions.newInstance();
                            }
                            else if(a.equals("5"))
                            {
                                selectedFragment = DropDownTypeQuestions.newInstance();
                            }

                            selectedFragment.setArguments(bundle);
                            transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_layout, selectedFragment);
                            transaction.addToBackStack("subcatsurvey");
                            transaction.commit();

                        }
                    });

                    final AlertDialog dialog = alert.create();

                    dialog.show();

        }
                else {
System.out.println("Else part qnsans");
                    selectedFragment = AddingQuestionToSubCat.newInstance();
                    selectedFragment.setArguments(bundle);

                    transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout, selectedFragment);
                    transaction.addToBackStack("subcatsurvey");
                    transaction.commit();

    }
}
        });


     /*   stringReturn.string_return(getActivity(), URLs.URL_GETTING_QNS+"?catid="+s1+"&subcatid="+s2, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println("kkkkkkkkkkkkkk"+result);

                try {
                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));
                        // System.out.println("lllllllllllllKGHGJGhhh"+jsonObject.getString("doc_type"));

                        movie = new Qnsbean(jsonObject.getString("quiz_qns"), jsonObject.getString("quiz_id"));
                        docList.add(movie);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });
*/

        return view;
    }
    public void post(){
        stringReturn.string_return(getActivity(), URLs.URL_GETTING_QNS+"?catid="+s1+"&subcatid="+s2+"&quizorsur="+HomeActivity.type+
                "&mailid="+sessionManager.getRegId("email"), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println("kkkkkkkkkkkkkk"+result);

                try {

                    JSONArray jsonArray =new JSONArray(result);
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));
                        // System.out.println("lllllllllllllKGHGJGhhh"+jsonObject.getString("doc_type"));

                        movie = new Qnsbean(jsonObject.getString("quiz_qns"), jsonObject.getString("quiz_id"));
                        docList.add(movie);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });

    }
}

