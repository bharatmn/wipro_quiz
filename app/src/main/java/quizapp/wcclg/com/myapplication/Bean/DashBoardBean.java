package quizapp.wcclg.com.myapplication.Bean;

/**
 * Created by Admin08 on 6/25/2018.
 */

public class DashBoardBean  {
    private String Email,Score,TimeTaken;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getScore() {
        return Score;
    }

    public void setScore(String score) {
        Score = score;
    }

    public String getTimeTaken() {
        return TimeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        TimeTaken = timeTaken;
    }

    public DashBoardBean(String Email, String Score,String Time) {
        this.Email = Email;
        this.Score = Score;
        this.TimeTaken=Time;
    }
}
