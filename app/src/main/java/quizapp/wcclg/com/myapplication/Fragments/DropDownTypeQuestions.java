package quizapp.wcclg.com.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Adapters.SubCatListAdapter;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleySingletonQuee;


/**
 * Created by vinod on 9/4/18.
 */

public class DropDownTypeQuestions extends Fragment {

    EditText question,answer,tool_tip;
    EditText option1type,option2type,option3type,option4type;
    Button submit;
    StringRequest stringRequest;
    String q_post,a_post,t_post;
    RadioGroup radioGroup;
    String s1= ModeratorAdapter.cat_id;
    //String s2= ModeratorAdapter.cat_id;
    Fragment selectedFragment = null;
    RadioButton opt1,opt2,opt3,opt4;
    JSONObject optionans ;

    public static DropDownTypeQuestions newInstance() {
        DropDownTypeQuestions itemOnFragment = new DropDownTypeQuestions();
        return itemOnFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drop_down_question_ask, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        question = view.findViewById(R.id.qnsText);

        radioGroup=view.findViewById(R.id.options);

        opt1=view.findViewById(R.id.opt1);
        opt2=view.findViewById(R.id.opt2);
        opt3=view.findViewById(R.id.opt3);
        opt4=view.findViewById(R.id.opt4);

        option1type=view.findViewById(R.id.textopt1);
        option2type=view.findViewById(R.id.textopt2);
        option3type=view.findViewById(R.id.textopt3);
        option4type=view.findViewById(R.id.textopt4);

        submit = view.findViewById(R.id.send);
        //This class awesome
        //  awesomeValidation.addValidation(this, R.id.tool_tipText1, RegexTemplate.NOT_EMPTY, R.string.app_name);



    view.setFocusableInTouchMode(true);
      view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");

                    Bundle bundle = getArguments();
                    String key = bundle.getString("back");

                    if(key.equals("1"))
                        getFragmentManager().popBackStack("homesurvey", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    else
                        getFragmentManager().popBackStack("subcatsurvey", FragmentManager.POP_BACK_STACK_INCLUSIVE);


                    return true;
                }
                return false;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // System.out.println("wwwwwwwwwww"+ !opt1.getText().toString().trim().equals("") );
                // Initialize a new StringRequest

                if(!question.getText().toString().trim().equals("") && radioGroup.getCheckedRadioButtonId()!=-1
                        && !option1type.getText().toString().trim().equals("")  && !option2type.getText().toString().trim().equals("")
                        && !option3type.getText().toString().trim().equals("") && !option4type.getText().toString().trim().equals("")) {

                    q_post = question.getText().toString().trim();

                    optionans= new JSONObject();
                    try {
                        optionans.put("opt1",option1type.getText().toString().trim());
                        optionans.put("opt2",option2type.getText().toString().trim());
                        optionans.put("opt3",option3type.getText().toString().trim());
                        optionans.put("opt4",option4type.getText().toString().trim());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //     t_post = "{ \"opt1\"" +opt1.getText().toString().trim().toLowerCase();

                    if(opt1.isChecked())
                    {
                        a_post="1";
                    }
                    else if(opt2.isChecked())
                    {
                        a_post="2";
                    }
                    else if(opt3.isChecked())
                    {
                        a_post="3";
                    }
                    else if(opt4.isChecked())
                    {
                        a_post="4";
                    }

                    // t_post = tool_tip.getText().toString().trim().toLowerCase();
                    stringRequest = new StringRequest(
                            Request.Method.POST,
                            URLs.URL_POSTING_QUESTION,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    System.out.println("kkkkkkkk" + response);
                                    Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // progressBar.setVisibility(View.INVISIBLE);
                                    System.out.println("direct qns unser cat" + error.toString());
                                    //                         System.out.println("eeeeeeeee" +Integer.toString( error.networkResponse.statusCode));

                                    //Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();


                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();

                            //System.out.println("appuboss"+s1+q_post+a_post+ SubCatListAdapter.selected_sub_catid);

                            params.put("catid", s1);
                            params.put("qns", q_post);
                            params.put("ans", a_post);
                           params.put("tip", optionans.toString());

                            if( SubCatListAdapter.selected_sub_catid == null) {
                                params.put("subcatid", "0");
                            }
                            else {
                                params.put("subcatid", SubCatListAdapter.selected_sub_catid);
                            }

                            params.put("qnsid","5");
                            params.put("quizorsur", HomeActivity.type);
                            // quizorsur

                            return params;
                        }
                    };
                    // Add StringRequest to the RequestQueue
                    VolleySingletonQuee.getInstance(getActivity()).addToRequestQueue(stringRequest);

                }
                else
                {
                    Toast.makeText(getActivity(),"Please type your Question and select Answer",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }
}

