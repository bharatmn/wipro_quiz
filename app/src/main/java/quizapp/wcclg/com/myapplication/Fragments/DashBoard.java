package quizapp.wcclg.com.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Adapters.DashBoardAdapter;
import quizapp.wcclg.com.myapplication.Adapters.ModeratorAdapter;
import quizapp.wcclg.com.myapplication.Adapters.SubCatListAdapter;
import quizapp.wcclg.com.myapplication.Bean.DashBoardBean;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.StringReturn;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;


/**
 * Created by vinod on 9/4/18.
 */

public class DashBoard extends Fragment {

    private RecyclerView recyclerView;
    private DashBoardAdapter mAdapter;
    DashBoardBean getset;
    StringReturn stringReturn;
    String q_post,a_post,t_post;
    //String s1= ModeratorAdapter.cat_id;
    private List<DashBoardBean> docList = new ArrayList<>();
    Fragment selectedFragment = null;

    public static DashBoard newInstance() {
        DashBoard itemOnFragment = new DashBoard();
        return itemOnFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard, container, false);
        getActivity().setTitle("DashBoard");
        setHasOptionsMenu(true);

      //  session = new SessionManager(getApplicationContext());

        recyclerView = view.findViewById(R.id.recycler_view_dashboard);

        mAdapter = new DashBoardAdapter(getActivity(),docList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
       // RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(view.getContext(), 2);
       // recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("ONBACK", "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("ONBACK", "onKey Back listener is working!!!");
                    getFragmentManager().popBackStack("users", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                    selectedFragment = TakeQUiz.newInstance();
//                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                    transaction.replace(R.id.frame_layout, selectedFragment);
//                    transaction.commit();

                    return true;
                }
                return false;
            }
        });


        stringReturn=new StringReturn();

        stringReturn.string_return(getActivity(), URLs.DASH_BOARD, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {

                try {
                    JSONArray jsonArray =new JSONArray(result);
                    System.out.println("lllllllllllllKGHGJGhhh"+result.toString());
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=new JSONObject(jsonArray.getString(i));

                        getset = new DashBoardBean(jsonObject.getString("email_id"),
                                jsonObject.getString("user_value"),jsonObject.getString("time_taken"));
                        docList.add(getset);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        });


        return view;
    }
}

