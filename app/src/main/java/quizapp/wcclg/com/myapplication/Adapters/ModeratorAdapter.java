package quizapp.wcclg.com.myapplication.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import quizapp.wcclg.com.myapplication.Activities.HomeActivity;
import quizapp.wcclg.com.myapplication.Activities.MainActivity;
import quizapp.wcclg.com.myapplication.Bean.Moderatorbean;
import quizapp.wcclg.com.myapplication.Fragments.SubcatlistUnderCat;
import quizapp.wcclg.com.myapplication.R;
import quizapp.wcclg.com.myapplication.Utils.Deleate_post;
import quizapp.wcclg.com.myapplication.Utils.URLs;
import quizapp.wcclg.com.myapplication.Utils.VolleyCallback;
import quizapp.wcclg.com.myapplication.Utils.VolleySingletonQuee;

/**
 * Created by vinod on 29/1/18.
 */

public class ModeratorAdapter extends RecyclerView.Adapter<ModeratorAdapter.MyViewHolder> {

    private List<Moderatorbean> docList;
        Activity activity;
    AlertDialog alertDialog;
    StringRequest stringRequest;
    String cat_name;
    public  static  String cat_id;
    Fragment selectedFragment = null;
    String[] randomcolor={"#00a1f1","#f65314","#ffbb00","#EC274E","#A4C639"};
    int [] randomicons={R.drawable.ic_question,R.drawable.ic_logic_board_games,R.drawable.ic_sun,R.drawable.ic_customer_service};
    int count;
    public  static  String selected_cat_id,RespStatus;
    public    String delected_cat_id;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, url;
        ImageView imageView;
        public CardView cardView;
        LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.title);
            imageView = view.findViewById(R.id.icon);
          // url = (TextView) view.findViewById(R.id.genre);
            cardView=view.findViewById(R.id.card_view);
            linearLayout=view.findViewById(R.id.line1);
        }
    }


    public ModeratorAdapter(Activity activity, List<Moderatorbean> moviesList) {
        this.docList = moviesList;
        this.activity=activity;
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doc_list_row, parent, false);

        int height = parent.getHeight() / 4;
        itemView.setMinimumHeight(height);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Moderatorbean doc = docList.get(position);
        holder.name.setText(doc.getCat_name());

        count++;
        System.out.println("Sequentialrepeating"+count);


            if (count >= randomicons.length) {
                count = 0;
                System.out.println("Sequentialrepeatingcountinside if" + count);


            }


        System.out.println("Sequentialrepeatingcountinside ifrandomcolor[count]" + randomcolor[count]);

        holder.imageView.setImageResource(randomicons[count]);

        if (doc.getCat_id().equals("cat_id")){
            holder.imageView.setImageResource(R.drawable.ic_icons8_plus);
            holder.linearLayout.setBackgroundColor(Color.parseColor("#dcdcdc"));
            holder.name.setText(doc.getCat_name());
            holder.name.setTextColor(Color.parseColor("#000000"));
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get prompts.xml view
                cat_id=doc.getCat_id();
                if (doc.getCat_id().equals("cat_id")){
                    LayoutInflater li = LayoutInflater.from(activity);
                    View promptsView = li.inflate(R.layout.forgotpassword, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            activity);

                    // set prompts.xml to alertdialog builder
                    alertDialogBuilder.setView(promptsView);
                    alertDialogBuilder.setIcon(R.drawable.category);
                    alertDialogBuilder.setTitle("Category");
                    final EditText userInput = promptsView
                            .findViewById(R.id.enter_email);


                    final Button userInputbutton = promptsView
                            .findViewById(R.id.send);


                    // create alert dialog
                    alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                    userInputbutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {

                            cat_name = userInput.getText().toString();

                            if(cat_name.equals(""))
                            {
                                Toast.makeText(activity, "Please enter Category Name", Toast.LENGTH_SHORT).show();
                            }

                            else {
                                // Initialize a new StringRequest
                                stringRequest = new StringRequest(
                                        Request.Method.POST,
                                        URLs.URL_GETTING_NEWCAT,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                System.out.println("kkkkkkkk" + response);
                                                alertDialog.dismiss();
                                                try {

                                                    JSONObject jsonObject = new JSONObject(response.toString());
                                                    RespStatus = jsonObject.getString("Status");
                                                    selected_cat_id = jsonObject.getString("cat_id");

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                Toast.makeText(activity, RespStatus, Toast.LENGTH_LONG).show();

                                                if (RespStatus.contains("Successfully")) {
                                                    selectedFragment = SubcatlistUnderCat.newInstance();
                                                    FragmentTransaction transaction = ((HomeActivity) activity).getSupportFragmentManager().beginTransaction();
                                                    transaction.replace(R.id.frame_layout, selectedFragment);
                                                    transaction.addToBackStack("category");
                                                    transaction.commit();
                                                }

                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                                //Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();


                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<>();

                                        params.put("quizorsur", HomeActivity.type);
                                        params.put("catname", cat_name);
                                        params.put("catid", "11");
                                        params.put("type", "1");
                                        params.put("mid", MainActivity.ModeratorId);

                                        return params;
                                    }
                                };
                                // Add StringRequest to the RequestQueue
                                VolleySingletonQuee.getInstance(activity).addToRequestQueue(stringRequest);
                            }
                        }
                    });
                }else {
                    System.out.println("ggggggggggggggg"+doc.getCat_id());
                    selected_cat_id=doc.getCat_id();

                    selectedFragment = SubcatlistUnderCat.newInstance();
                    FragmentTransaction transaction = ((HomeActivity) activity).getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout, selectedFragment);
                    transaction.addToBackStack("category");
                    transaction.commit();
                }
            }
        });



        holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                delected_cat_id=doc.getCat_id();

                if(delected_cat_id.equals("cat_id"))
                {
                 return true;
                }

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        activity);
                LayoutInflater li = LayoutInflater.from(activity);
                View promptsView = li.inflate(R.layout.deleate_dialog, null);
                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
                final TextView userInput = promptsView
                        .findViewById(R.id.deleate);



                // create alert dialog
                alertDialog = alertDialogBuilder.create();

                // show itToast
                alertDialog.show();

                userInput.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("llllllllllllldeleate"+delected_cat_id);
                        alertDialog.dismiss();

                        Deleate_post.deleate_item_post(activity,"1",delected_cat_id, "0","0", new VolleyCallback() {
                            @Override
                            public void onSuccessResponse(String result) {
                                docList.remove(position);
                                notifyDataSetChanged();
                            }
                        });

                    }
                });

                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return docList.size();
    }
}